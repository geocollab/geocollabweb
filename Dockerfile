FROM docker.io/library/node:18-alpine AS builder

RUN apk add --update git npm \
  && rm -rf /var/cache/apk/*
RUN npm install -g @angular/cli

WORKDIR /app
COPY . /app
RUN npm install
RUN npm run build

FROM docker.io/library/nginx:1.21-alpine
WORKDIR /usr/share/nginx/html
COPY docker/nginx.conf /etc/nginx/nginx.conf
COPY --from=builder /app/dist/geocollabweb /usr/share/nginx/html
