import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserSettingsComponent } from './user-settings.component';
import { UserSettingsService } from '../services/user-settings.service';
import { AuthService } from '../core/auth.service';
import { UserService } from '../services/user.service';
import { provideHttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { User } from '../entity/database/user';
import { UserSettings } from '../entity/database/usersettings';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

const fakeUser: User = {
  id: 1,
  created_at: '',
  updated_at: '',
  oidc_uid: '',
  name: 'testuser',
  email: '',
  role: '',
};

describe('UserSettingsComponent', () => {
  let component: UserSettingsComponent;
  let fixture: ComponentFixture<UserSettingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [UserSettingsComponent, NoopAnimationsModule],
      providers: [
        provideHttpClient(),
        {
          provide: AuthService,
          useValue: { identityClaims: { preferred_username: 'testuser' } },
        },
        {
          provide: UserService,
          useValue: {
            getUserByName(): Observable<User> {
              return of(fakeUser);
            },
          },
        },
        {
          provide: UserSettingsService,
          useValue: {
            getUserSettingsById(): Observable<UserSettings> {
              return of({
                id: 1,
                created_at: '',
                updated_at: '',
                user: fakeUser,
                darkmode: false,
                statistics_database: 0,
                geocacher: 0,
              });
            },
          },
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(UserSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
