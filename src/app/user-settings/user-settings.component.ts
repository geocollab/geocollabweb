import { ChangeDetectionStrategy, Component, viewChild } from '@angular/core';
import { UserSettingsService } from '../services/user-settings.service';
import { MatButtonModule } from '@angular/material/button';
import { AuthService } from '../core/auth.service';
import { User } from '../entity/database/user';
import { UserSettings } from '../entity/database/usersettings';
import { MatAccordion, MatExpansionModule } from '@angular/material/expansion';
import { UserService } from '../services/user.service';
import { Observable, of, Subject } from 'rxjs';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-user-settings',
  standalone: true,
  imports: [MatButtonModule, MatExpansionModule, CommonModule],
  templateUrl: './user-settings.component.html',
  styleUrl: './user-settings.component.sass',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserSettingsComponent {
  user: Observable<User> = new Subject();
  userSettings?: UserSettings;
  accordion = viewChild.required(MatAccordion);

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private userSettingsService: UserSettingsService,
  ) {
    this.getUser();
    this.getUserSettings();
  }

  getUser(): void {
    this.userService
      .getUserByName(this.authService.identityClaims['preferred_username'])
      .subscribe(
        (user) => {
          this.accordion().closeAll();
          this.user = of(user);
          this.accordion().openAll();
        },
        (error) => {
          console.log(error);
        },
      );
  }

  getUserSettings(): void {
    this.userSettingsService.getUserSettingsById(1).subscribe(
      (userSettings) => {
        this.userSettings = userSettings;
      },
      (error) => {
        console.error(error);
      },
    );
  }
}
