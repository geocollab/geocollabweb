import { TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { provideRouter } from '@angular/router';
import { Subject } from 'rxjs';
import {
  OAuthEvent,
  OAuthService,
  OAuthSuccessEvent,
} from 'angular-oauth2-oidc';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

describe('AppComponent', () => {
  let mockOAuthEvents: Subject<OAuthEvent>;
  let mockOAuthService: jasmine.SpyObj<OAuthService>;

  beforeEach(async () => {
    mockOAuthEvents = new Subject<OAuthEvent>();
    mockOAuthService = jasmine.createSpyObj<OAuthService>(
      {
        configure: void 0,
        hasValidAccessToken: false,
        loadDiscoveryDocument: Promise.resolve(
          new OAuthSuccessEvent('discovery_document_loaded'),
        ),
        loadDiscoveryDocumentAndLogin: Promise.resolve(false),
        loadDiscoveryDocumentAndTryLogin: Promise.resolve(false),
        loadUserProfile: Promise.resolve({}),
        restartSessionChecksIfStillLoggedIn: void 0,
        setupAutomaticSilentRefresh: void 0,
        silentRefresh: Promise.resolve(
          new OAuthSuccessEvent('silently_refreshed'),
        ),
        stopAutomaticRefresh: void 0,
        tryLogin: Promise.resolve(false),
        tryLoginCodeFlow: Promise.resolve(void 0),
        tryLoginImplicitFlow: Promise.resolve(false),
      },
      {
        events: mockOAuthEvents.asObservable(),
      },
    );
    await TestBed.configureTestingModule({
      providers: [
        provideRouter([]),
        { provide: OAuthService, useValue: mockOAuthService },
      ],
      imports: [AppComponent, NoopAnimationsModule],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'geocollabweb'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('geocollabweb');
  });
});
