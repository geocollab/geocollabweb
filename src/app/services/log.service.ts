import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class LogService {
  private apiUrl = 'https://geocollab.docker.localhost/v1/log';

  constructor(private http: HttpClient) {}

  getLogById(id: number) {
    return this.http.get(this.apiUrl + `/${id}`);
  }
}
