import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class StatisticsRunService {
  private apiUrl = 'https://geocollab.docker.localhost/v1/statistics/run';

  constructor(private http: HttpClient) {}

  getStatisticsById(id: number) {
    return this.http.get(this.apiUrl + `/${id}`);
  }
}
