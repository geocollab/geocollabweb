import { TestBed } from '@angular/core/testing';

import { GeocacherService } from './geocacher.service';
import { provideHttpClient } from '@angular/common/http';

describe('GeocacherService', () => {
  let service: GeocacherService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideHttpClient()],
    });
    service = TestBed.inject(GeocacherService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
