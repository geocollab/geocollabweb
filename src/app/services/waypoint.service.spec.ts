import { TestBed } from '@angular/core/testing';

import { WaypointService } from './waypoint.service';
import { provideHttpClient } from '@angular/common/http';

describe('WaypointService', () => {
  let service: WaypointService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideHttpClient()],
    });
    service = TestBed.inject(WaypointService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
