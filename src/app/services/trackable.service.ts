import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class TrackableService {
  private apiUrl = 'https://geocollab.docker.localhost/v1/trackable';

  constructor(private http: HttpClient) {}

  getTrackableByNumber(id: string) {
    return this.http.get(this.apiUrl + `/${id}`);
  }
}
