import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class StatusService {
  private apiUrl = 'https://geocollab.docker.localhost/v1/status';

  constructor(private http: HttpClient) {}

  getStatus() {
    return this.http.get(this.apiUrl);
  }
}
