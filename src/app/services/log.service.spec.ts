import { TestBed } from '@angular/core/testing';

import { LogService } from './log.service';
import { provideHttpClient } from '@angular/common/http';

describe('LogService', () => {
  let service: LogService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideHttpClient()],
    });
    service = TestBed.inject(LogService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
