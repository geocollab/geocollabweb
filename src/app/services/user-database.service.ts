import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class UserDatabaseService {
  private apiUrl = 'https://geocollab.docker.localhost/v1/database';

  constructor(private http: HttpClient) {}

  getUserDatabaseById(id: number) {
    return this.http.get(this.apiUrl + `/${id}`);
  }
}
