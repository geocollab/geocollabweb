import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserSettings } from '../entity/database/usersettings';

@Injectable({
  providedIn: 'root',
})
export class UserSettingsService {
  private apiUrl = 'https://geocollab.docker.localhost/v1/usersettings';

  constructor(private http: HttpClient) {}

  getUserSettingsById(id: number) {
    return this.http.get<UserSettings>(this.apiUrl + `/${id}`);
  }
}
