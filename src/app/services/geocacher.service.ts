import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class GeocacherService {
  private apiUrl = 'https://geocollab.docker.localhost/v1/geocacher';

  constructor(private http: HttpClient) {}

  getGeocacherByName(name: string) {
    return this.http.get(this.apiUrl + `/${name}`);
  }
}
