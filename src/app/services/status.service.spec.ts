import { TestBed } from '@angular/core/testing';

import { StatusService } from './status.service';
import { provideHttpClient } from '@angular/common/http';

describe('StatusService', () => {
  let service: StatusService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideHttpClient()],
    });
    service = TestBed.inject(StatusService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
