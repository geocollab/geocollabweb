import { TestBed } from '@angular/core/testing';

import { TrackableService } from './trackable.service';
import { provideHttpClient } from '@angular/common/http';

describe('TrackableService', () => {
  let service: TrackableService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideHttpClient()],
    });
    service = TestBed.inject(TrackableService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
