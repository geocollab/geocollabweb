import { TestBed } from '@angular/core/testing';

import { GeocacheService } from './geocache.service';
import { provideHttpClient } from '@angular/common/http';

describe('GeocacheService', () => {
  let service: GeocacheService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideHttpClient()],
    });
    service = TestBed.inject(GeocacheService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
