import { TestBed } from '@angular/core/testing';

import { UserDatabaseService } from './user-database.service';
import { provideHttpClient } from '@angular/common/http';

describe('UserDatabaseService', () => {
  let service: UserDatabaseService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideHttpClient()],
    });
    service = TestBed.inject(UserDatabaseService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
