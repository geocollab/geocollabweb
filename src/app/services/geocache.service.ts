import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class GeocacheService {
  private apiUrl = 'https://geocollab.docker.localhost/v1/geocache';

  constructor(private http: HttpClient) {}

  getGeocacheByGuid(guid: string) {
    return this.http.get(this.apiUrl + `/${guid}`);
  }
}
