import { TestBed } from '@angular/core/testing';

import { StatisticsRunService } from './statistics-run.service';
import { provideHttpClient } from '@angular/common/http';

describe('StatisticsRunService', () => {
  let service: StatisticsRunService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideHttpClient()],
    });
    service = TestBed.inject(StatisticsRunService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
