import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../entity/database/user';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private apiUrl = 'https://geocollab.docker.localhost/v1/user';

  constructor(private http: HttpClient) {}

  getUserByName(name: string) {
    return this.http.get<User>(this.apiUrl + `/${name}`, {});
  }
}
