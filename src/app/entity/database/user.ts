import { Model } from './model';

export interface User extends Model {
  oidc_uid: string;
  name: string;
  email: string;
  role: string;
}
