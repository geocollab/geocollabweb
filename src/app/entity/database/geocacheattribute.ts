import { Geocache } from './geocache';
import { Model } from './model';
import { GeoAttribute } from './attribute';

export interface GeocacheAttribute extends Model {
  geocache: Geocache;
  attribute: GeoAttribute;
  inc: number;
}
