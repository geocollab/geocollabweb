import { Model } from './model';

export interface GeoAttribute extends Model {
  name: string;
}
