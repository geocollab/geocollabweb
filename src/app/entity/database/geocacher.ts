import { Model } from './model';

export interface Geocacher extends Model {
  name: string;
}
