export interface Model {
  id: number;
  created_at: Date | string;
  updated_at: Date | string;
}
