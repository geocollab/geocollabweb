import { Model } from './model';
import { User } from './user';

export interface UserSettings extends Model {
  user: User;
  darkmode: boolean;
  statistics_database: number;
  geocacher: number;
}
