import { Model } from './model';

export interface Trackable extends Model {
  number: string;
  trackable_type: string;
}
