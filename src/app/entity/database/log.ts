import { Model } from './model';

export interface Log extends Model {
  geocache: number;
  log_type: string;
  log_date: Date;
  associated_geocacher: number;
  text: string;
  is_text_encoded: boolean;
}
