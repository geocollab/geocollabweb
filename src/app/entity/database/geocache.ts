import { Model } from './model';
import { User } from './user';
import { UserDatabase } from './userdatabase';
import { Geocacher } from './geocacher';
import { Log } from './log';
import { Waypoint } from './waypoint';

export interface Geocache extends Model {
  user: User;
  database: UserDatabase;
  guid: string;
  gc_code: string;
  name: string;
  type: string;
  short_description: string;
  long_description: string;
  difficulty: number;
  terrain: number;
  size: string;
  owner_string: string;
  owner: Geocacher;
  encoded_hint: string;
  favorite_points: number;
  groundspeak_id: number;
  premium_member_only: boolean;
  archived: boolean;
  logs: Log[];
  waypoints: Waypoint[];
}
