import { Model } from './model';

export interface Waypoint extends Model {
  waypoint_type: string;
  name: string;
  prefix: string;
  description: string;
  latitude: number;
  longitude: number;
  height: number;
  geocache: number;
}
