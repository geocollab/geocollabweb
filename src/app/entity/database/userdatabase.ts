import { Model } from './model';
import { User } from './user';

export interface UserDatabase extends Model {
  name: string;
  user: User;
  insert_mode: string;
}
