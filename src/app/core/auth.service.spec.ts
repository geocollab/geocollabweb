import { TestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';
import { Subject } from 'rxjs';
import {
  OAuthEvent,
  OAuthService,
  OAuthSuccessEvent,
} from 'angular-oauth2-oidc';

describe('AuthService', () => {
  let mockOAuthEvents: Subject<OAuthEvent>;
  let mockOAuthService: jasmine.SpyObj<OAuthService>;
  let service: AuthService;

  beforeEach(() => {
    mockOAuthEvents = new Subject<OAuthEvent>();
    mockOAuthService = jasmine.createSpyObj<OAuthService>(
      {
        configure: void 0,
        hasValidAccessToken: false,
        loadDiscoveryDocument: Promise.resolve(
          new OAuthSuccessEvent('discovery_document_loaded'),
        ),
        loadDiscoveryDocumentAndLogin: Promise.resolve(false),
        loadDiscoveryDocumentAndTryLogin: Promise.resolve(false),
        loadUserProfile: Promise.resolve({}),
        restartSessionChecksIfStillLoggedIn: void 0,
        setupAutomaticSilentRefresh: void 0,
        silentRefresh: Promise.resolve(
          new OAuthSuccessEvent('silently_refreshed'),
        ),
        stopAutomaticRefresh: void 0,
        tryLogin: Promise.resolve(false),
        tryLoginCodeFlow: Promise.resolve(void 0),
        tryLoginImplicitFlow: Promise.resolve(false),
      },
      {
        events: mockOAuthEvents.asObservable(),
      },
    );

    TestBed.configureTestingModule({
      providers: [
        AuthService,
        { provide: OAuthService, useValue: mockOAuthService },
      ],
    });
    service = TestBed.inject(AuthService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
