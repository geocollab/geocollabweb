import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { filter, switchMap, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';

@Injectable()
export class AuthGuardWithForcedLogin {
  constructor(private authService: AuthService) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<boolean> {
    return this.authService.isDoneLoading$.pipe(
      filter((isDone) => isDone),
      // switchMap((_) => this.authService.isAuthenticated$),
      switchMap(() => this.authService.isAuthenticated$),
      tap(
        (isAuthenticated) =>
          isAuthenticated || this.authService.login(state.url),
      ),
    );
  }
}
