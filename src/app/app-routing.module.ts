import { Routes } from '@angular/router';
import { UserSettingsComponent } from './user-settings/user-settings.component';
import { DashboardComponent } from './dashboard/dashboard.component';

export const routes: Routes = [
  { path: 'user/settings', component: UserSettingsComponent },
  { path: 'dashboard', component: DashboardComponent },
];
