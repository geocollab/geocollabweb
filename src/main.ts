import { bootstrapApplication, BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app/app.component';
import { provideRouter, withViewTransitions } from '@angular/router';

import { routes } from './app/app-routing.module';
import { CommonModule } from '@angular/common';
import { APP_INITIALIZER, importProvidersFrom } from '@angular/core';
import { provideAnimations } from '@angular/platform-browser/animations';
import {
  provideHttpClient,
  withInterceptorsFromDi,
} from '@angular/common/http';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import { AuthConfig, provideOAuthClient } from 'angular-oauth2-oidc';
import { authAppInitializerFactory } from './app/core/auth-app-initializer.factory';
import { AuthService } from './app/core/auth.service';
import { authModuleConfig, authConfig } from './app/core/auth-config';

bootstrapApplication(AppComponent, {
  providers: [
    provideRouter(routes, withViewTransitions()),
    importProvidersFrom(CommonModule, BrowserModule),
    provideAnimations(),
    provideHttpClient(withInterceptorsFromDi()),
    provideOAuthClient(authModuleConfig),
    provideAnimationsAsync(),
    { provide: AuthConfig, useValue: authConfig },
    {
      provide: APP_INITIALIZER,
      useFactory: authAppInitializerFactory,
      deps: [AuthService],
      multi: true,
    },
  ],
});
