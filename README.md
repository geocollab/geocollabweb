# GeoCollabWeb

Web-App which only work with GeoCollabServer to be usefull.

Goals of the client side application:

- Behave like a PWA application.
- Enable a user to work offline.
- Don't use multiple technologies, stick to one. We will use React
- Generate the REST-API Client with the [Open-API-Spec](https://www.openapis.org/). The specification is
